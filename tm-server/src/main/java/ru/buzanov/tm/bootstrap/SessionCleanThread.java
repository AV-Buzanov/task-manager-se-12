package ru.buzanov.tm.bootstrap;

import org.jetbrains.annotations.NotNull;
import ru.buzanov.tm.api.ServiceLocator;
import ru.buzanov.tm.entity.Session;

import java.sql.SQLException;
import java.util.Properties;

public class SessionCleanThread extends Thread {
    @NotNull
    private final ServiceLocator serviceLocator;

    public SessionCleanThread(@NotNull final ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
        this.properties = serviceLocator.getSessionService().getProperties();
    }

    private Properties properties;

    @Override
    public void run() {
        while (true) {
            try {
                sleep(Integer.parseInt(properties.getProperty("lifeTime")) * 2);
                for (Session session : serviceLocator.getSessionService().findAll()) {
                    long timeDif = System.currentTimeMillis() - session.getCreateDate().getTime();
                    if (timeDif > Integer.parseInt(properties.getProperty("lifeTime")))
                        serviceLocator.getSessionService().remove(session.getId());
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
