package ru.buzanov.tm.bootstrap;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.buzanov.tm.api.ServiceLocator;
import ru.buzanov.tm.api.repository.IProjectRepository;
import ru.buzanov.tm.api.repository.ISessionRepository;
import ru.buzanov.tm.api.repository.ITaskRepository;
import ru.buzanov.tm.api.repository.IUserRepository;
import ru.buzanov.tm.api.service.IProjectService;
import ru.buzanov.tm.api.service.ISessionService;
import ru.buzanov.tm.api.service.ITaskService;
import ru.buzanov.tm.api.service.IUserService;
import ru.buzanov.tm.endpoint.*;
import ru.buzanov.tm.repository.ProjectRepository;
import ru.buzanov.tm.repository.SessionRepository;
import ru.buzanov.tm.repository.TaskRepository;
import ru.buzanov.tm.repository.UserRepository;
import ru.buzanov.tm.service.ProjectService;
import ru.buzanov.tm.service.SessionService;
import ru.buzanov.tm.service.TaskService;
import ru.buzanov.tm.service.UserService;
import ru.buzanov.tm.util.DatabaseUtil;

import javax.xml.ws.Endpoint;
import java.io.BufferedReader;
import java.io.InputStreamReader;

@Getter
@Setter
@NoArgsConstructor
public final class Bootstrap implements ServiceLocator {
    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();
    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();
    @NotNull
    private final IUserRepository userRepository = new UserRepository();
    @NotNull
    private final ISessionRepository sessionRepository = new SessionRepository();
    @NotNull
    private final IProjectService projectService = new ProjectService();
    @NotNull
    private final ITaskService taskService = new TaskService();
    @NotNull
    private final IUserService userService = new UserService();
    @NotNull
    private final ISessionService sessionService = new SessionService();
    @NotNull
    private final String adress = "http://0.0.0.0:8080/";

    public void start() {
        Thread sessionCleanThread = new SessionCleanThread(this);
        sessionCleanThread.setDaemon(true);
        sessionCleanThread.start();
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        Endpoint.publish(adress + "ProjectEndpoint?wsdl", new ProjectEndpoint(this));
        Endpoint.publish(adress + "TaskEndpoint?wsdl", new TaskEndpoint(this));
        Endpoint.publish(adress + "AdminUserEndpoint?wsdl", new AdminUserEndpoint(this));
        Endpoint.publish(adress + "SessionEndpoint?wsdl", new SessionEndpoint(this));
        Endpoint.publish(adress + "UserEndpoint?wsdl", new UserEndpoint(this));
        System.out.println("Task manager server is running.");
        System.out.println(adress + "ProjectEndpoint?wsdl");
        System.out.println(adress + "TaskEndpoint?wsdl");
        System.out.println(adress + "AdminUserEndpoint?wsdl");
        System.out.println(adress + "SessionEndpoint?wsdl");
        System.out.println(adress + "UserEndpoint?wsdl");

        while (true) {
            try {
                if ("exit".equals(reader.readLine())) {
                    System.exit(0);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}