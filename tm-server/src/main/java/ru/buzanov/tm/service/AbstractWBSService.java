package ru.buzanov.tm.service;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.buzanov.tm.api.repository.IWBSRepository;
import ru.buzanov.tm.api.service.IWBSService;
import ru.buzanov.tm.entity.AbstractWBS;
import ru.buzanov.tm.enumerated.Field;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@NoArgsConstructor
public abstract class AbstractWBSService<T extends AbstractWBS>implements IWBSService<T> {

    @Nullable
    public abstract T load(@Nullable final String userId, @Nullable T entity) throws Exception;

    @Override
    public abstract void load(@Nullable String userId, List<T> list) throws Exception;

    @NotNull
    public abstract Collection<T> findAll(@Nullable final String userId) throws Exception;

    @NotNull
    public abstract Collection<T> findByName(@Nullable final String userId, @Nullable final String name) throws Exception;

    @NotNull
    public abstract Collection<T> findByDescription(@Nullable final String userId, @Nullable final String desc) throws Exception;

    @Nullable
    public abstract T findOne(@Nullable final String userId, @Nullable final String id) throws Exception;

    public abstract boolean isNameExist(@Nullable final String userId, @Nullable final String name) throws Exception;

    public abstract void merge(@Nullable final String userId, @Nullable final String id, @Nullable final T entity) throws Exception;

    public abstract  @NotNull Collection<T> findAllOrdered(@Nullable String userId, boolean dir, @NotNull Field field) throws Exception;
    @Nullable
    public abstract T remove(@Nullable final String userId, @Nullable final String id) throws Exception;

    public abstract void removeAll(@Nullable final String userId) throws Exception;

    @Nullable
    public String getList(@Nullable final String userId) throws Exception {
        if (userId == null || userId.isEmpty())
            return null;
        int indexBuf = 1;
        StringBuilder s = new StringBuilder();
        Collection<T> list = findAll(userId);
        for (T entity : list) {
            s.append(indexBuf).append(". ").append(entity.getName());
            if (list.size() > indexBuf)
                s.append(System.getProperty("line.separator"));
            indexBuf++;
        }
        return s.toString();
    }

    @Nullable
    public String getIdByCount(@Nullable final String userId, int count) throws Exception {
        if (userId == null || userId.isEmpty())
            return null;
        int indexBuf = 1;
        Collection<T> list = findAll(userId);
        for (T entity : list) {
            if (indexBuf == count)
                return entity.getId();
            indexBuf++;
        }
        return null;
    }
}
