package ru.buzanov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.buzanov.tm.api.repository.IRepository;
import ru.buzanov.tm.api.service.ISessionService;
import ru.buzanov.tm.entity.Project;
import ru.buzanov.tm.entity.Session;
import ru.buzanov.tm.repository.ProjectRepository;
import ru.buzanov.tm.repository.SessionRepository;
import ru.buzanov.tm.util.DatabaseUtil;

import java.sql.SQLException;
import java.util.Collection;
import java.util.List;

public class SessionService extends AbstractService<Session> implements ISessionService {

    @Nullable
    @Override
    public Session load(@Nullable Session session) throws Exception {
        if (session == null || session.getId() == null || session.getUserId() == null || session.getSignature() == null)
            return null;
        @NotNull final SessionRepository sessionRepository = openTransaction();
        if (sessionRepository.getConnection() == null)
            throw new Exception("Database connection error.");
        try {
            if (sessionRepository.findOne(session.getId()) == null)
                sessionRepository.load(session);
            else
                sessionRepository.merge(session.getId(), session);
            sessionRepository.getConnection().commit();
            return sessionRepository.findOne(session.getId());
        } catch (SQLException e) {
            sessionRepository.getConnection().rollback();
            throw new Exception("SQL error");
        } finally {
            sessionRepository.getConnection().close();
        }
    }

    @Override
    public @NotNull Collection<Session> findAll() throws Exception {
        SessionRepository sessionRepository = openTransaction();
        if (sessionRepository.getConnection() == null)
            throw new Exception("Database connection error.");
        try {
            return sessionRepository.findAll();
        } catch (SQLException e) {
            throw new Exception("SQL error");
        } finally {
            sessionRepository.getConnection().close();
        }
    }

    @Override
    public void load(@Nullable List<Session> list) throws Exception {
        if (list == null)
            throw new Exception("Argument cant be empty or null.");
        for (Session session : list)
            load(session);
    }

    @Nullable
    @Override
    public Session findOne(@Nullable String id) throws Exception {
        if (id == null || id.isEmpty())
            return null;
        SessionRepository projectRepository = openTransaction();
        if (projectRepository.getConnection() == null)
            throw new Exception("Database connection error.");
        try {
            return projectRepository.findOne(id);
        } catch (SQLException e) {
            throw new Exception("SQL error");
        } finally {
            projectRepository.getConnection().close();
        }
    }

    @Override
    public void merge(@Nullable String id, @Nullable Session session) throws Exception {
        if (session == null || session.getId() == null)
            throw new Exception("Argument can't be empty or null");
        if (id == null || id.isEmpty())
            throw new Exception("Argument can't be empty or null");
        SessionRepository projectRepository = openTransaction();
        if (projectRepository.getConnection() == null)
            throw new Exception("Database connection error.");
        try {
            if (projectRepository.findOne(id) != null)
                projectRepository.merge(id, session);
            projectRepository.getConnection().commit();
        } catch (SQLException e) {
            projectRepository.getConnection().rollback();
            throw new Exception("SQL error");
        } finally {
            projectRepository.getConnection().close();
        }
    }

    @Nullable
    @Override
    public Session remove(@Nullable String id) throws Exception {
        if (id == null || id.isEmpty())
            throw new Exception("Argument can't be empty or null");
        SessionRepository sessionRepository = openTransaction();
        if (sessionRepository.getConnection() == null)
            throw new Exception("Database connection error.");
        try {
            Session session = sessionRepository.findOne(id);
            sessionRepository.remove(id);
            sessionRepository.getConnection().commit();
            return session;
        } catch (SQLException e) {
            sessionRepository.getConnection().rollback();
            throw new Exception("SQL error");
        } finally {
            sessionRepository.getConnection().close();
        }
    }

    @Override
    public void removeAll() throws Exception {
        SessionRepository sessionRepository = openTransaction();
        if (sessionRepository.getConnection() == null)
            throw new Exception("Database connection error.");
        try {
            sessionRepository.removeAll();
            sessionRepository.getConnection().commit();
        } catch (SQLException e) {
            sessionRepository.getConnection().rollback();
            throw new Exception("SQL error");
        } finally {
            sessionRepository.getConnection().close();
        }
    }

    @NotNull
    private SessionRepository openTransaction() {
        SessionRepository sessionRepository = new SessionRepository();
        sessionRepository.setConnection(DatabaseUtil.getConnection());
        return sessionRepository;
    }
}
