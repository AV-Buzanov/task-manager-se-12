package ru.buzanov.tm.service;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.buzanov.tm.api.repository.IRepository;
import ru.buzanov.tm.api.service.IService;
import ru.buzanov.tm.entity.AbstractEntity;
import ru.buzanov.tm.repository.AbstractRepository;
import ru.buzanov.tm.repository.AbstractWBSRepository;
import ru.buzanov.tm.repository.UserRepository;
import ru.buzanov.tm.util.DatabaseUtil;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Collection;
import java.util.List;
import java.util.Properties;

@NoArgsConstructor
public abstract class AbstractService<T extends AbstractEntity> implements IService<T> {
    @NotNull
    private final static Properties properties = new Properties();

    static {
        try {
            properties.load(AbstractService.class.getResourceAsStream("/session.properties"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Nullable
    public abstract T load(@Nullable final T entity) throws Exception;

    @NotNull
    public abstract Collection<T> findAll() throws Exception;

    public abstract void load(@Nullable final List<T> list) throws Exception;

    @Nullable
    public abstract T findOne(@Nullable final String id) throws Exception;

    public abstract void merge(@Nullable final String id, @Nullable final T entity) throws Exception;

    @Nullable
    public abstract T remove(@Nullable final String id) throws Exception;

    public abstract void removeAll() throws Exception;

    @NotNull
    public Properties getProperties() {
        return properties;
    }

}
