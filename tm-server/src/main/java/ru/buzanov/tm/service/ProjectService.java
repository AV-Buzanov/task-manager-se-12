package ru.buzanov.tm.service;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.buzanov.tm.api.repository.IProjectRepository;
import ru.buzanov.tm.api.repository.IWBSRepository;
import ru.buzanov.tm.api.service.IProjectService;
import ru.buzanov.tm.entity.Project;
import ru.buzanov.tm.enumerated.Field;
import ru.buzanov.tm.repository.ProjectRepository;
import ru.buzanov.tm.util.DatabaseUtil;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

@NoArgsConstructor
public class ProjectService extends AbstractWBSService<Project> implements IProjectService {

    @Nullable
    @Override
    public Project load(@Nullable String userId, @Nullable Project project) throws Exception {
        if (project == null || project.getId() == null || project.getName() == null)
            return null;
        if (userId == null || userId.isEmpty())
            return null;
        project.setUserId(userId);
        project.setCreateDate(new Date(System.currentTimeMillis()));
       @NotNull final ProjectRepository projectRepository = openTransaction();
        if (projectRepository.getConnection() == null)
            throw new Exception("Database connection error.");
        try {
            if (projectRepository.isNameExist(userId, project.getName()))
                throw new Exception("This name already exist");
            if (projectRepository.findOne(userId, project.getId()) == null)
                projectRepository.load(project);
            else
                projectRepository.merge(userId, project.getId(), project);
            projectRepository.getConnection().commit();
            return projectRepository.findOne(userId, project.getId());
        } catch (SQLException e) {
            projectRepository.getConnection().rollback();
            throw new Exception("SQL error");
        } finally {
            projectRepository.getConnection().close();
        }
    }

    @Override
    public void load(@Nullable String userId, List<Project> list) throws Exception {
        for (Project project : list)
            load(userId, project);
    }

    @Override
    public @NotNull Collection<Project> findAll(@Nullable String userId) throws Exception {
        if (userId == null || userId.isEmpty())
            return new ArrayList<>();
        ProjectRepository projectRepository = openTransaction();
        if (projectRepository.getConnection() == null)
            throw new Exception("Database connection error.");
        try {
            return projectRepository.findAll(userId);
        } catch (SQLException e) {
            throw new Exception("SQL error");
        } finally {
            projectRepository.getConnection().close();
        }
    }

    @Override
    public @NotNull Collection<Project> findByName(@Nullable String userId, @Nullable String name) throws Exception {
        if (userId == null || userId.isEmpty())
            return new ArrayList<>();
        if (name == null || name.isEmpty())
            return new ArrayList<>();
        ProjectRepository projectRepository = openTransaction();
        if (projectRepository.getConnection() == null)
            throw new Exception("Database connection error.");
        try {
            return projectRepository.findByName(userId, name);
        } catch (SQLException e) {
            throw new Exception("SQL error");
        } finally {
            projectRepository.getConnection().close();
        }
    }

    @Override
    public @NotNull Collection<Project> findByDescription(@Nullable String userId, @Nullable String desc) throws Exception {
        if (userId == null || userId.isEmpty())
            return new ArrayList<>();
        if (desc == null || desc.isEmpty())
            return new ArrayList<>();
        ProjectRepository projectRepository = openTransaction();
        if (projectRepository.getConnection() == null)
            throw new Exception("Database connection error.");
        try {
            return projectRepository.findByDescription(userId, desc);
        } catch (SQLException e) {
            throw new Exception("SQL error");
        } finally {
            projectRepository.getConnection().close();
        }
    }

    @Nullable
    @Override
    public Project findOne(@Nullable String userId, @Nullable String id) throws Exception {
        if (userId == null || userId.isEmpty())
            return null;
        if (id == null || id.isEmpty())
            return null;
        ProjectRepository projectRepository = openTransaction();
        if (projectRepository.getConnection() == null)
            throw new Exception("Database connection error.");
        try {
            return projectRepository.findOne(userId, id);
        } catch (SQLException e) {
            throw new Exception("SQL error");
        } finally {
            projectRepository.getConnection().close();
        }
    }

    @Override
    public boolean isNameExist(@Nullable String userId, @Nullable String name) throws Exception {
        if (userId == null || userId.isEmpty())
            throw new Exception("Argument can't be empty or null");
        if (name == null || name.isEmpty())
            throw new Exception("Argument can't be empty or null");
        ProjectRepository projectRepository = openTransaction();
        if (projectRepository.getConnection() == null)
            throw new Exception("Database connection error.");
        try {
            return projectRepository.isNameExist(userId, name);
        } catch (SQLException e) {
            throw new Exception("SQL error");
        } finally {
            projectRepository.getConnection().close();
        }
    }

    @Override
    public void merge(@Nullable String userId, @Nullable String id, @Nullable Project project) throws Exception {
        if (project == null || project.getName() == null)
            throw new Exception("Argument can't be empty or null");
        if (userId == null || userId.isEmpty())
            throw new Exception("Argument can't be empty or null");
        if (id == null || id.isEmpty())
            throw new Exception("Argument can't be empty or null");
        ProjectRepository projectRepository = openTransaction();
        if (projectRepository.getConnection() == null)
            throw new Exception("Database connection error.");
        try {
            if (projectRepository.findOne(id) != null)
                projectRepository.merge(userId, id, project);
            projectRepository.getConnection().commit();
        } catch (SQLException e) {
            projectRepository.getConnection().rollback();
            throw new Exception("SQL error");
        } finally {
            projectRepository.getConnection().close();
        }
    }

    @Override
    public @NotNull Collection<Project> findAllOrdered(@Nullable String userId, boolean dir, @NotNull Field field) throws Exception {
        if (userId == null || userId.isEmpty())
            return new ArrayList<>();
        if (field == null)
            return new ArrayList<>();
        ProjectRepository projectRepository = openTransaction();
        if (projectRepository.getConnection() == null)
            throw new Exception("Database connection error.");
        try {
            return projectRepository.findAllOrdered(userId, dir, field);
        } catch (SQLException e) {
            throw new Exception("SQL error");
        } finally {
            projectRepository.getConnection().close();
        }
    }

    @Nullable
    @Override
    public Project remove(@Nullable String userId, @Nullable String id) throws Exception {
        if (userId == null || userId.isEmpty())
            throw new Exception("Argument can't be empty or null");
        if (id == null || id.isEmpty())
            throw new Exception("Argument can't be empty or null");
        ProjectRepository projectRepository = openTransaction();
        if (projectRepository.getConnection() == null)
            throw new Exception("Database connection error.");
        try {
            Project project = projectRepository.findOne(userId, id);
            projectRepository.remove(userId, id);
            projectRepository.getConnection().commit();
            return project;
        } catch (SQLException e) {
            projectRepository.getConnection().rollback();
            throw new Exception("SQL error");
        } finally {
            projectRepository.getConnection().close();
        }
    }

    @Override
    public void removeAll(@Nullable String userId) throws Exception {
        if (userId == null || userId.isEmpty())
            throw new Exception("Argument can't be empty or null");
        ProjectRepository projectRepository = openTransaction();
        if (projectRepository.getConnection() == null)
            throw new Exception("Database connection error.");
        try {
            projectRepository.removeAll(userId);
            projectRepository.getConnection().commit();
        } catch (SQLException e) {
            projectRepository.getConnection().rollback();
            throw new Exception("SQL error");
        } finally {
            projectRepository.getConnection().close();
        }
    }

    @NotNull
    private ProjectRepository openTransaction() {
        ProjectRepository projectRepository = new ProjectRepository();
        projectRepository.setConnection(DatabaseUtil.getConnection());
        return projectRepository;
    }
}