package ru.buzanov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.buzanov.tm.enumerated.Field;

import java.sql.SQLException;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public interface IWBSService<T> {

    @NotNull Collection<T> findAll(@Nullable final String userId) throws Exception;

    @Nullable T findOne(@Nullable final String userId, @Nullable final String id) throws Exception;

    boolean isNameExist(@Nullable final String userId, @Nullable final String name) throws Exception;

    @Nullable T load(@Nullable final String userId, @Nullable final T entity) throws Exception;

    void load(@Nullable final String userId, final List<T> list) throws Exception;

    @Nullable String getList(@Nullable final String userId) throws Exception;

    @Nullable String getIdByCount(@Nullable final String userId, final int count) throws Exception;

    void merge(@Nullable final String userId, @Nullable final String id, @Nullable final T project) throws Exception;

    @Nullable T remove(@Nullable final String userId, @Nullable final String id) throws Exception;

    void removeAll(@Nullable final String userId) throws Exception;

    @NotNull Collection<T> findByDescription(@Nullable final String userId, @Nullable final String desc) throws Exception;

    @NotNull Collection<T> findByName(@Nullable final String userId, @Nullable final String name) throws Exception;

    @NotNull Collection<T> findAllOrdered(@Nullable String userId, boolean dir, @NotNull Field field) throws Exception;
}
