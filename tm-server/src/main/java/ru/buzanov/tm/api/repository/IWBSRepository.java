package ru.buzanov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.buzanov.tm.enumerated.Field;

import java.sql.SQLException;
import java.util.Collection;

public interface IWBSRepository<T> extends IRepository<T> {

    @NotNull Collection<T> findAll(@NotNull final String userId) throws SQLException;

    @Nullable T findOne(@NotNull final String userId, @NotNull final String id) throws SQLException;

    boolean isNameExist(@NotNull final String userId, @NotNull final String name) throws SQLException;

    void merge(@NotNull final String userId, @NotNull final String id, @NotNull final T project) throws SQLException;

    @Nullable T remove(@NotNull final String userId, @NotNull final String id) throws SQLException;

    void removeAll(@NotNull final String userId) throws SQLException;

    @NotNull Collection<T> findByDescription(@NotNull final String userId, @NotNull final String desc) throws SQLException;

    @NotNull Collection<T> findByName(@NotNull final String userId, @NotNull final String name) throws SQLException;

    @NotNull Collection<T> findAllOrdered(@NotNull String userId, boolean dir, @NotNull Field field) throws SQLException;

}
