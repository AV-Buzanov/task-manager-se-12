package ru.buzanov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import ru.buzanov.tm.entity.Task;

import java.sql.SQLException;
import java.util.Collection;

public interface ITaskRepository extends IWBSRepository<Task> {

    @NotNull Collection<Task> findByProjectId(@NotNull final String projectId) throws SQLException;

    void removeByProjectId(@NotNull final String projectId) throws SQLException;

    @NotNull Collection<Task> findByProjectId(@NotNull final String userId, @NotNull final String projectId) throws SQLException;

    void removeByProjectId(@NotNull final String userId, @NotNull final String projectId) throws SQLException;
}
