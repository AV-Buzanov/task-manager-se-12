package ru.buzanov.tm.api.repository;

import ru.buzanov.tm.entity.Session;

public interface ISessionRepository extends IRepository<Session> {

}
