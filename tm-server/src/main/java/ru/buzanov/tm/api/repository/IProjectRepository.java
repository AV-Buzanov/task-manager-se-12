package ru.buzanov.tm.api.repository;

import ru.buzanov.tm.entity.Project;

public interface IProjectRepository extends IWBSRepository<Project> {
}

