package ru.buzanov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.buzanov.tm.entity.User;
import ru.buzanov.tm.enumerated.RoleType;

import java.sql.SQLException;
import java.util.Collection;

public interface IUserRepository extends IRepository<User> {

    @Nullable User findByLogin(@NotNull final String login) throws SQLException;

    boolean isPassCorrect(@NotNull final String login, @NotNull final String pass) throws SQLException;

    boolean isLoginExist(@NotNull final String login) throws SQLException;

    @NotNull Collection<User> findByRole(@NotNull final RoleType role) throws SQLException;
}
