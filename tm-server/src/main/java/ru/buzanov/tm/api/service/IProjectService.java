package ru.buzanov.tm.api.service;

import ru.buzanov.tm.entity.Project;

public interface IProjectService extends IWBSService<Project> {

}
