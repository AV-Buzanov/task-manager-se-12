package ru.buzanov.tm.api.service;

import org.jetbrains.annotations.Nullable;
import ru.buzanov.tm.entity.User;
import ru.buzanov.tm.enumerated.RoleType;

import java.sql.SQLException;
import java.util.Collection;

public interface IUserService extends IService<User> {

    @Nullable User findByLogin(@Nullable String login) throws Exception;

    boolean isPassCorrect(@Nullable final String login, @Nullable final String pass) throws Exception;

    boolean isLoginExist(@Nullable final String login) throws Exception;

    @Nullable Collection<User> findByRole(@Nullable final RoleType role) throws Exception;
}
