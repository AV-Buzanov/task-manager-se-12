package ru.buzanov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.sql.SQLException;
import java.util.Collection;
import java.util.List;
import java.util.Properties;

public interface IService<T> {

    @NotNull Collection<T> findAll() throws Exception;

    @Nullable T findOne(@Nullable final String id) throws Exception;

    void merge(@Nullable final String id, @Nullable final T entity) throws Exception;

    @Nullable T remove(@Nullable final String id) throws Exception;

    void removeAll() throws Exception;

    @Nullable T load(@Nullable final T entity) throws Exception;

    void load(final List<T> list) throws Exception;

    @NotNull Properties getProperties();
}
