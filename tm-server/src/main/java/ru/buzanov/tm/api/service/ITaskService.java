package ru.buzanov.tm.api.service;

import org.jetbrains.annotations.Nullable;
import ru.buzanov.tm.entity.Task;

import java.sql.SQLException;
import java.util.Collection;

public interface ITaskService extends IWBSService<Task> {

    @Nullable Collection<Task> findByProjectId(@Nullable final String projectId) throws Exception;

    void removeByProjectId(@Nullable final String projectId) throws Exception;

    @Nullable Collection<Task> findByProjectId(@Nullable final String userId, @Nullable final String projectId) throws Exception;

    void removeByProjectId(@Nullable final String userId, @Nullable final String projectId) throws Exception;
}
