package ru.buzanov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.sql.SQLException;
import java.util.Collection;
import java.util.List;

public interface IRepository<T> {
    @NotNull Collection<T> findAll() throws SQLException;

    @Nullable T findOne(@NotNull final String id) throws SQLException;

    void merge(@NotNull final String id, @NotNull final T entity) throws SQLException;

    @Nullable T remove(@NotNull final String id) throws SQLException;

    void removeAll() throws SQLException;

    @Nullable T load(@NotNull final T entity) throws SQLException;

    void load(final List<T> list) throws SQLException;
}
