package ru.buzanov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.buzanov.tm.api.ServiceLocator;
import ru.buzanov.tm.entity.Session;
import ru.buzanov.tm.entity.User;
import ru.buzanov.tm.enumerated.RoleType;

import javax.jws.WebMethod;
import javax.jws.WebService;
import java.util.Collection;

@WebService
public class AdminUserEndpoint extends AbstractEndpoint {

    public AdminUserEndpoint(ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @WebMethod
    @Nullable
    public User findByLogin(@Nullable Session session, @Nullable String login) throws Exception {
        auth(session);
        if (!userService.findOne(session.getUserId()).getRoleType().equals(RoleType.ADMIN))
            throw new Exception("This command for admin only.");
        return userService.findByLogin(login);
    }

    @WebMethod
    @Nullable
    public Collection<User> findByRole(@Nullable Session session, @Nullable RoleType role) throws Exception {
        auth(session);
        if (!userService.findOne(session.getUserId()).getRoleType().equals(RoleType.ADMIN))
            throw new Exception("This command for admin only.");
        return userService.findByRole(role);
    }

    @WebMethod
    @NotNull
    public Collection<User> findAll(@Nullable Session session) throws Exception {
        auth(session);
        if (!userService.findOne(session.getUserId()).getRoleType().equals(RoleType.ADMIN))
            throw new Exception("This command for admin only.");
        return userService.findAll();
    }

    @WebMethod
    @Nullable
    public User findOneAdmin(@Nullable Session session, @Nullable String id) throws Exception {
        auth(session);
        if (!userService.findOne(session.getUserId()).getRoleType().equals(RoleType.ADMIN))
            throw new Exception("This command for admin only.");
        return userService.findOne(id);
    }

    @WebMethod
    @Nullable
    public User removeAdmin(@Nullable Session session, @Nullable String id) throws Exception {
        auth(session);
        if (!userService.findOne(session.getUserId()).getRoleType().equals(RoleType.ADMIN))
            throw new Exception("This command for admin only.");
        if (userService.findOne(id).getRoleType().equals(RoleType.ADMIN))
            throw new Exception("You can't remove admin.");
        return userService.remove(id);
    }
}
