package ru.buzanov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.buzanov.tm.api.ServiceLocator;
import ru.buzanov.tm.entity.Session;
import ru.buzanov.tm.util.SignatureUtil;

import javax.jws.WebMethod;
import javax.jws.WebService;
import java.util.Date;

@WebService
public class SessionEndpoint extends AbstractEndpoint {
    public SessionEndpoint(ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @WebMethod
    @Nullable
    public Session getSession(@Nullable final String login, @Nullable final String passwordHash) throws Exception {
        if (login == null || login.isEmpty() || passwordHash == null || passwordHash.isEmpty())
            throw new Exception("empty login or password");
        if (!userService.isLoginExist(login))
            throw new Exception("Login doesn't exist.");
        if (!userService.isPassCorrect(login, passwordHash))
            throw new Exception("Invalid password.");
        @NotNull final Session session = new Session();
        session.setUserId(userService.findByLogin(login).getId());
        @Nullable final String signature = SignatureUtil.sign(session,
                properties.getProperty("signSalt"),
                Integer.parseInt(properties.getProperty("signCycle")));
        session.setSignature(signature);
        sessionService.load(session);
        return session;
    }

    @WebMethod
    @Nullable
    public Session updateSession(@Nullable final Session session) throws Exception {
        auth(session);
        session.setSignature(null);
        session.setCreateDate(new Date(System.currentTimeMillis()));
        @Nullable final String signature = SignatureUtil.sign(session,
                properties.getProperty("signSalt"),
                Integer.parseInt(properties.getProperty("signCycle")));
        session.setSignature(signature);
        sessionService.merge(session.getId(), session);
        return session;
    }

    @WebMethod
    public void killSession(@Nullable Session session) throws Exception {
        auth(session);
        sessionService.remove(session.getId());
    }
}
