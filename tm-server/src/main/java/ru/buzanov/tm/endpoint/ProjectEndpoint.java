package ru.buzanov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.buzanov.tm.api.ServiceLocator;
import ru.buzanov.tm.entity.Project;
import ru.buzanov.tm.entity.Session;
import ru.buzanov.tm.enumerated.Field;

import javax.jws.WebMethod;
import javax.jws.WebService;
import java.util.Collection;
import java.util.List;


@WebService
public class ProjectEndpoint extends AbstractEndpoint {

    public ProjectEndpoint(ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @WebMethod
    @NotNull
    public Collection<Project> findAllP(@Nullable Session session) throws Exception {
        auth(session);
        return projectService.findAll(session.getUserId());
    }

    @WebMethod
    @Nullable
    public Project findOneP(@Nullable Session session, @Nullable String id) throws Exception {
        auth(session);
        return projectService.findOne(session.getUserId(), id);
    }

    @WebMethod
    public boolean isNameExistP(@Nullable Session session, @Nullable String name) throws Exception {
        auth(session);
        return projectService.isNameExist(session.getUserId(), name);
    }

    @WebMethod
    @Nullable
    public String getListP(@Nullable Session session) throws Exception {
        auth(session);
        return projectService.getList(session.getUserId());
    }

    @WebMethod
    @Nullable
    public String getIdByCountP(@Nullable Session session, int count) throws Exception {
        auth(session);
        return projectService.getIdByCount(session.getUserId(), count);
    }

    public void mergeP(@Nullable Session session, @Nullable String id, @Nullable Project project) throws Exception {
        auth(session);
        projectService.merge(session.getUserId(), id, project);
    }

    @WebMethod
    @Nullable
    public Project removeP(@Nullable Session session, @Nullable String id) throws Exception {
        auth(session);
        return projectService.remove(session.getUserId(), id);
    }

    public void removeAllP(@Nullable Session session) throws Exception {
        auth(session);
        projectService.removeAll(session.getUserId());
    }

    @WebMethod
    @NotNull
    public Collection<Project> findByDescriptionP(@Nullable Session session, @Nullable String desc) throws Exception {
        auth(session);
        return projectService.findByDescription(session.getUserId(), desc);
    }

    @WebMethod
    @NotNull
    public Collection<Project> findByNameP(@Nullable Session session, @Nullable String name) throws Exception {
        auth(session);
        return projectService.findByName(session.getUserId(), name);
    }

    @WebMethod
    public @NotNull Collection<Project> findAllOrderedP(@Nullable Session session, boolean dir, @NotNull Field field) throws Exception {
        auth(session);
        return projectService.findAllOrdered(session.getUserId(), dir, field);
    }

    @WebMethod
    @Nullable
    public Project loadP(@Nullable Session session, @Nullable Project entity) throws Exception {
        auth(session);
        return projectService.load(session.getUserId(), entity);
    }

    @WebMethod
    public void loadListP(@Nullable Session session, List<Project> list) throws Exception {
        auth(session);
        projectService.load(session.getUserId(), list);
    }
}
