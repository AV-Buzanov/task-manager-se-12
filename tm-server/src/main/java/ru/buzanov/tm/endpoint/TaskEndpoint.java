package ru.buzanov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.buzanov.tm.api.ServiceLocator;
import ru.buzanov.tm.entity.Session;
import ru.buzanov.tm.entity.Task;
import ru.buzanov.tm.enumerated.Field;

import javax.jws.WebMethod;
import javax.jws.WebService;
import java.util.Collection;
import java.util.List;

@WebService
public class TaskEndpoint extends AbstractEndpoint {

    public TaskEndpoint(ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @WebMethod
    @Nullable
    public Collection<Task> findByProjectIdT(@Nullable Session session, @Nullable String projectId) throws Exception {
        auth(session);
        return taskService.findByProjectId(session.getUserId(), projectId);
    }

    @WebMethod
    public void removeByProjectIdT(@Nullable Session session, @Nullable String projectId) throws Exception {
        auth(session);
        taskService.removeByProjectId(session.getUserId(), projectId);
    }

    @WebMethod
    @NotNull
    public Collection<Task> findAllT(@Nullable Session session) throws Exception {
        auth(session);
        return taskService.findAll(session.getUserId());
    }

    @WebMethod
    @NotNull
    public Collection<Task> findAllOrderedT(@Nullable Session session, boolean dir, @NotNull Field field) throws Exception {
        auth(session);
        return taskService.findAllOrdered(session.getUserId(), dir, field);
    }

    @WebMethod
    @Nullable
    public Task findOneT(@Nullable Session session, @Nullable String id) throws Exception {
        auth(session);
        return taskService.findOne(session.getUserId(), id);
    }

    @WebMethod
    public boolean isNameExistT(@Nullable Session session, @Nullable String name) throws Exception {
        auth(session);
        return taskService.isNameExist(session.getUserId(), name);
    }

    @WebMethod
    @Nullable
    public String getListT(@Nullable Session session) throws Exception {
        auth(session);
        return taskService.getList(session.getUserId());
    }

    @WebMethod
    @Nullable
    public String getIdByCountT(@Nullable Session session, int count) throws Exception {
        auth(session);
        return taskService.getIdByCount(session.getUserId(), count);
    }

    @WebMethod
    public void mergeT(@Nullable Session session, @Nullable String id, @Nullable Task project) throws Exception {
        auth(session);
        taskService.merge(session.getUserId(), id, project);
    }

    @WebMethod
    @Nullable
    public Task removeT(@Nullable Session session, @Nullable String id) throws Exception {
        auth(session);
        return taskService.remove(session.getUserId(), id);
    }

    @WebMethod
    public void removeAllT(@Nullable Session session) throws Exception {
        auth(session);
        taskService.removeAll(session.getUserId());
    }

    @WebMethod
    @NotNull
    public Collection<Task> findByDescriptionT(@Nullable Session session, @Nullable String desc) throws Exception {
        auth(session);
        return taskService.findByDescription(session.getUserId(), desc);
    }

    @WebMethod
    @NotNull
    public Collection<Task> findByNameT(@Nullable Session session, @Nullable String name) throws Exception {
        auth(session);
        return taskService.findByName(session.getUserId(), name);
    }

    @WebMethod
    @Nullable
    public Task loadT(@Nullable Session session, @Nullable Task entity) throws Exception {
        return taskService.load(session.getUserId(), entity);
    }

    @WebMethod
    public void loadListT(@Nullable Session session, List<Task> list) throws Exception {
        taskService.load(session.getUserId(), list);
    }
}
