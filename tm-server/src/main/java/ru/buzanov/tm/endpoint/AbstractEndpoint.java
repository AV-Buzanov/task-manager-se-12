package ru.buzanov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.buzanov.tm.api.ServiceLocator;
import ru.buzanov.tm.api.service.IProjectService;
import ru.buzanov.tm.api.service.ISessionService;
import ru.buzanov.tm.api.service.ITaskService;
import ru.buzanov.tm.api.service.IUserService;
import ru.buzanov.tm.entity.Session;
import ru.buzanov.tm.util.SignatureUtil;

import java.util.Properties;

public abstract class AbstractEndpoint {
    @NotNull
    final IProjectService projectService;
    @NotNull
    final ITaskService taskService;
    @NotNull
    final IUserService userService;
    @NotNull
    final ISessionService sessionService;
    @NotNull
    final ServiceLocator serviceLocator;
    @NotNull
    final Properties properties;

    public AbstractEndpoint(@NotNull final ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
        this.projectService = serviceLocator.getProjectService();
        this.taskService = serviceLocator.getTaskService();
        this.userService = serviceLocator.getUserService();
        this.sessionService = serviceLocator.getSessionService();
        this.properties = sessionService.getProperties();
    }

    protected void auth(@Nullable final Session session) throws Exception {
        if (session == null)
            throw new Exception("null session");
        long timeDif = System.currentTimeMillis() - session.getCreateDate().getTime();
        if (timeDif > Integer.parseInt(properties.getProperty("lifeTime"))) {
            sessionService.remove(session.getId());
            throw new Exception("Session time out.");
        }
        @Nullable final String signature = session.getSignature();
        session.setSignature(null);
        @Nullable final String ourSignature = SignatureUtil.sign(session,
                properties.getProperty("signSalt"),
                Integer.parseInt(properties.getProperty("signCycle")));
        if (signature == null || !signature.equals(ourSignature))
            throw new Exception("Invalid session signature.");
        if (sessionService.findOne(session.getId()) == null)
            throw new Exception("Session not found.");

        System.out.print(userService.findOne(session.getUserId()).getLogin() + "  ");
        System.out.print(Thread.currentThread().getStackTrace()[2].getClassName() + "  ");
        System.out.print(Thread.currentThread().getStackTrace()[2].getMethodName());
        System.out.println();
    }
}
