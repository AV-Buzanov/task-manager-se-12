package ru.buzanov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.buzanov.tm.api.ServiceLocator;
import ru.buzanov.tm.entity.Session;
import ru.buzanov.tm.entity.User;

import javax.jws.WebMethod;
import javax.jws.WebService;

@WebService
public class UserEndpoint extends AbstractEndpoint {
    public UserEndpoint(ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @WebMethod
    @Nullable
    public User findOne(@Nullable Session session) throws Exception {
        auth(session);
        return userService.findOne(session.getUserId());
    }

    @WebMethod
    public void merge(@Nullable Session session, @Nullable User user) throws Exception {
        auth(session);
        if (user == null)
            throw new Exception("Null");
        userService.merge(session.getUserId(), user);
    }

    @WebMethod
    public boolean isLoginExist(@Nullable String login) throws Exception {
        return userService.isLoginExist(login);
    }

    @WebMethod
    public void remove(@Nullable Session session) throws Exception {
        auth(session);
        userService.remove(session.getUserId());
    }

    @WebMethod
    public void registryUser(@Nullable final String login, @Nullable final String passwordHash) throws Exception {
        if (login == null || login.isEmpty() || userService.isLoginExist(login))
            throw new Exception("This login already exist.");
        if (passwordHash == null || passwordHash.isEmpty() || passwordHash.length() < 6)
            throw new Exception("Invalid password.");
        @NotNull final User user = new User();
        user.setLogin(login);
        user.setPasswordHash(passwordHash);
        userService.load(user);
    }
}
