package ru.buzanov.tm.repository;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.buzanov.tm.api.repository.IUserRepository;
import ru.buzanov.tm.entity.User;
import ru.buzanov.tm.enumerated.RoleType;
import ru.buzanov.tm.util.DatabaseUtil;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@NoArgsConstructor
public class UserRepository extends AbstractRepository<User> implements IUserRepository {

    @Nullable
    public User findByLogin(@NotNull final String login) throws SQLException {
        @NotNull final String sql = "SELECT*FROM user WHERE login=?";
        @NotNull final PreparedStatement preparedStatement = connection.prepareStatement(sql);
        preparedStatement.setString(1, login);
        @NotNull final ResultSet resultSet = preparedStatement.executeQuery();
        return fetch(resultSet);
    }

    @NotNull
    public Collection<User> findByRole(@NotNull final RoleType role) throws SQLException {
        @NotNull final String sql = "SELECT*FROM user WHERE roleType=?";
        @NotNull final Collection<User> list = new ArrayList<>();
        @NotNull final PreparedStatement preparedStatement = connection.prepareStatement(sql);
        preparedStatement.setString(1, role.toString());
        @NotNull final ResultSet resultSet = preparedStatement.executeQuery();
        User user;
        do {
            user = fetch(resultSet);
            if (user != null) list.add(user);
        } while (user != null);
        return list;
    }

    public boolean isPassCorrect(@NotNull final String login, @NotNull final String pass) throws SQLException {
        User user = findByLogin(login);
        if (user != null && user.getPasswordHash() != null) {
            String s = user.getPasswordHash();
            return pass.equals(s);
        }
        return false;
    }

    public boolean isLoginExist(@NotNull final String login) throws SQLException {
        return (findByLogin(login) != null);
    }

    public void merge(@NotNull final String id, @NotNull final User user) throws SQLException {
        @NotNull final StringBuilder sql = new StringBuilder("UPDATE user SET name=?, login=?, password=?, roleType=? WHERE id=?");
        @NotNull final PreparedStatement preparedStatement = connection.prepareStatement(sql.toString());
        preparedStatement.setString(1, user.getName());
        preparedStatement.setString(2, user.getLogin());
        preparedStatement.setString(3, user.getPasswordHash());
        preparedStatement.setString(4, user.getRoleType().toString());
        preparedStatement.setString(5, id);
        preparedStatement.executeUpdate();
    }

    @Nullable
    @Override
    public User load(@NotNull User user) throws SQLException {
        @NotNull final String sql = "INSERT INTO user (id, login, password, name) VALUES (?,?,?,?)";
        @NotNull final PreparedStatement preparedStatement = connection.prepareStatement(sql);
        preparedStatement.setString(1, user.getId());
        preparedStatement.setString(2, user.getLogin());
        preparedStatement.setString(3, user.getPasswordHash());
        preparedStatement.setString(4, user.getName());
        preparedStatement.executeUpdate();
        return user;
    }

    @Override
    public void load(@NotNull List<User> list) throws SQLException {
        for (User user : list) {
            load(user);
        }
    }

    @Override
    public @NotNull Collection<User> findAll() throws SQLException {
        @NotNull final String sql = "SELECT*FROM user";
        @NotNull final Collection<User> list = new ArrayList<>();
        @NotNull final PreparedStatement preparedStatement = connection.prepareStatement(sql);
        @NotNull final ResultSet resultSet = preparedStatement.executeQuery();
        User user;
        do {
            user = fetch(resultSet);
            if (user != null) list.add(user);
        } while (user != null);
        return list;
    }

    @Nullable
    @Override
    public User findOne(@NotNull String id) throws SQLException {
        @NotNull final String sql = "SELECT*FROM user WHERE id=?";
        @NotNull final PreparedStatement preparedStatement = connection.prepareStatement(sql);
        preparedStatement.setString(1, id);
        @NotNull final ResultSet resultSet = preparedStatement.executeQuery();
        return fetch(resultSet);
    }

    @Nullable
    @Override
    public User remove(@NotNull String id) throws SQLException {
        @NotNull final String sql = "DELETE FROM user WHERE id=?";
        User user = findOne(id);
        @NotNull final PreparedStatement preparedStatement = connection.prepareStatement(sql);
        preparedStatement.setString(1, id);
        preparedStatement.executeUpdate();
        return user;
    }

    @Override
    public void removeAll() throws SQLException {
        @NotNull final String sql = "DELETE FROM user";
        @NotNull final PreparedStatement preparedStatement = connection.prepareStatement(sql);
        preparedStatement.executeUpdate();
    }

    @Nullable
    private User fetch(@NotNull ResultSet resultSet) throws SQLException {
        @NotNull final User user = new User();
        if (resultSet.next()) {
            user.setId(resultSet.getString("id"));
            user.setName(resultSet.getString("name"));
            user.setLogin(resultSet.getString("login"));
            user.setPasswordHash(resultSet.getString("password"));
            user.setRoleType(RoleType.valueOf(resultSet.getString("roleType")));
            return user;
        }
        return null;
    }
}
