package ru.buzanov.tm.repository;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.buzanov.tm.api.repository.IProjectRepository;
import ru.buzanov.tm.entity.Project;
import ru.buzanov.tm.enumerated.Field;
import ru.buzanov.tm.enumerated.Status;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

@NoArgsConstructor
public class ProjectRepository extends AbstractWBSRepository<Project> implements IProjectRepository {
    @Nullable
    @Override
    public Project load(@NotNull Project project) throws SQLException {
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        @NotNull final String sql = "INSERT INTO project (id, name, description, createDate, startDate, finishDate, userId, status) VALUES (?,?,?,?,?,?,?,?)";
        @NotNull final PreparedStatement preparedStatement = connection.prepareStatement(sql);
        preparedStatement.setString(1, project.getId());
        preparedStatement.setString(2, project.getName());
        preparedStatement.setString(3, project.getDescription());
        preparedStatement.setTimestamp(4, timestamp);
        timestamp = null;
        if (project.getStartDate() != null) timestamp = new Timestamp(project.getStartDate().getTime());
        preparedStatement.setTimestamp(5, timestamp);
        timestamp = null;
        if (project.getFinishDate() != null) timestamp = new Timestamp(project.getFinishDate().getTime());
        preparedStatement.setTimestamp(6, timestamp);
        preparedStatement.setString(7, project.getUserId());
        preparedStatement.setString(8, project.getStatus().toString());
        preparedStatement.executeUpdate();
        return project;
    }

    @Override
    public void load(@NotNull List<Project> list) throws SQLException {
        for (@NotNull final Project project : list) {
            load(project);
        }
    }

    public void merge(@NotNull final String id, @NotNull final Project project) throws SQLException {
        Timestamp timestamp;
        @NotNull final StringBuilder sql = new StringBuilder("UPDATE project SET name=?, startDate=?, finishDate=?, description=?, status=? WHERE id=?");
        @NotNull final PreparedStatement preparedStatement = connection.prepareStatement(sql.toString());
        preparedStatement.setString(1, project.getName());
        timestamp = null;
        if (project.getStartDate() != null) timestamp = new Timestamp(project.getStartDate().getTime());
        preparedStatement.setTimestamp(2, timestamp);
        timestamp = null;
        if (project.getFinishDate() != null) timestamp = new Timestamp(project.getFinishDate().getTime());
        preparedStatement.setTimestamp(3, timestamp);
        preparedStatement.setString(4, project.getDescription());
        if (project.getDescription() != null)
            preparedStatement.setString(5, project.getStatus().toString());
        else preparedStatement.setString(5, null);
        preparedStatement.setString(6, id);
        preparedStatement.executeUpdate();
    }

    public void merge(@NotNull final String userId, @NotNull final String id, @NotNull final Project project) throws SQLException {
        Timestamp timestamp;
        @NotNull final StringBuilder sql = new StringBuilder("UPDATE project SET name=?, startDate=?, finishDate=?, description=?, status=? WHERE id=? AND userId=?");
        @NotNull final PreparedStatement preparedStatement = connection.prepareStatement(sql.toString());
        preparedStatement.setString(1, project.getName());
        timestamp = null;
        if (project.getStartDate() != null) timestamp = new Timestamp(project.getStartDate().getTime());
        preparedStatement.setTimestamp(2, timestamp);
        timestamp = null;
        if (project.getFinishDate() != null) timestamp = new Timestamp(project.getFinishDate().getTime());
        preparedStatement.setTimestamp(3, timestamp);
        preparedStatement.setString(4, project.getDescription());
        if (project.getDescription() != null)
            preparedStatement.setString(5, project.getStatus().toString());
        else preparedStatement.setString(5, null);
        preparedStatement.setString(6, id);
        preparedStatement.setString(7, userId);
        preparedStatement.executeUpdate();
    }

    @Nullable
    @Override
    public Project findOne(@NotNull String id) throws SQLException {
        @NotNull final String sql = "SELECT*FROM project WHERE id=?";
        @NotNull final PreparedStatement preparedStatement = connection.prepareStatement(sql);
        preparedStatement.setString(1, id);
        ResultSet resultSet = preparedStatement.executeQuery();
        return fetch(resultSet);
    }

    @Nullable
    @Override
    public Project findOne(@NotNull String userId, @NotNull String id) throws SQLException {
        @NotNull final String sql = "SELECT*FROM project WHERE userId=? AND id=?";
        @NotNull final PreparedStatement preparedStatement = connection.prepareStatement(sql);
        preparedStatement.setString(1, userId);
        preparedStatement.setString(2, id);
        ResultSet resultSet = preparedStatement.executeQuery();
        return fetch(resultSet);
    }

    @Override
    public @NotNull Collection<Project> findAll() throws SQLException {
        @NotNull final String sql = "SELECT*FROM project";
        @NotNull final Collection<Project> list = new ArrayList<>();
        @NotNull final PreparedStatement preparedStatement = connection.prepareStatement(sql);
        @NotNull final ResultSet resultSet = preparedStatement.executeQuery();
        Project project;
        do {
            project = fetch(resultSet);
            if (project != null) list.add(project);
        } while (project != null);
        return list;
    }

    @Override
    public @NotNull Collection<Project> findAll(@NotNull String userId) throws SQLException {
        @NotNull final String sql = "SELECT*FROM project WHERE userId=?";
        @NotNull final Collection<Project> list = new ArrayList<>();
        @NotNull final PreparedStatement preparedStatement = connection.prepareStatement(sql);
        preparedStatement.setString(1, userId);
        @NotNull final ResultSet resultSet = preparedStatement.executeQuery();
        Project project;
        do {
            project = fetch(resultSet);
            if (project != null) list.add(project);
        } while (project != null);
        return list;
    }

    public @NotNull Collection<Project> findAllOrdered(@NotNull String userId, boolean dir, @NotNull Field field) throws
            SQLException {
        @NotNull final StringBuilder sql = new StringBuilder("SELECT*FROM project WHERE userId=? ORDER BY ?");
        if (!dir) sql.append(" DESC");
        @NotNull final Collection<Project> list = new ArrayList<>();
        @NotNull final PreparedStatement preparedStatement = connection.prepareStatement(sql.toString());
        preparedStatement.setString(1, userId);
        preparedStatement.setString(2, field.displayName());
        @NotNull final ResultSet resultSet = preparedStatement.executeQuery();
        Project project;
        do {
            project = fetch(resultSet);
            if (project != null) list.add(project);
        } while (project != null);
        return list;
    }

    @Override
    public @NotNull Collection<Project> findByName(@NotNull String userId, @NotNull String name) throws
            SQLException {
        @NotNull final String sql = "SELECT*FROM project WHERE userId=? AND name LIKE ?";
        @NotNull final Collection<Project> list = new ArrayList<>();
        @NotNull final PreparedStatement preparedStatement = connection.prepareStatement(sql);
        preparedStatement.setString(1, userId);
        preparedStatement.setString(2, "%" + name + "%");
        @NotNull final ResultSet resultSet = preparedStatement.executeQuery();
        Project project;
        do {
            project = fetch(resultSet);
            if (project != null) list.add(project);
        } while (project != null);
        return list;
    }

    @Nullable
    @Override
    public Project findName(@NotNull String userId, @NotNull String name) throws SQLException {
        @NotNull final String sql = "SELECT*FROM project WHERE userId=? AND name=?";
        @NotNull final PreparedStatement preparedStatement = connection.prepareStatement(sql);
        preparedStatement.setString(1, userId);
        preparedStatement.setString(2, name);
        @NotNull final ResultSet resultSet = preparedStatement.executeQuery();
        return fetch(resultSet);
    }

    @Override
    public @NotNull Collection<Project> findByDescription(@NotNull String userId, @NotNull String desc) throws
            SQLException {
        @NotNull final String sql = "SELECT*FROM project WHERE userId=? AND description LIKE ?";
        @NotNull final Collection<Project> list = new ArrayList<>();
        @NotNull final PreparedStatement preparedStatement = connection.prepareStatement(sql);
        preparedStatement.setString(1, userId);
        preparedStatement.setString(2, "%" + desc + "%");
        @NotNull final ResultSet resultSet = preparedStatement.executeQuery();
        Project project;
        do {
            project = fetch(resultSet);
            if (project != null) list.add(project);
        } while (project != null);
        return list;
    }

    @Nullable
    @Override
    public Project remove(@NotNull String id) throws SQLException {
        @NotNull final String sql = "DELETE FROM project WHERE id=?";
        final Project project = findOne(id);
        @NotNull final PreparedStatement preparedStatement = connection.prepareStatement(sql);
        preparedStatement.setString(1, id);
        preparedStatement.executeUpdate();
        return project;
    }

    @Nullable
    @Override
    public Project remove(@NotNull String userId, @NotNull String id) throws SQLException {
        @NotNull final String sql = "DELETE FROM project WHERE userId=? AND id=?";
        final Project project = findOne(userId, id);
        @NotNull final PreparedStatement preparedStatement = connection.prepareStatement(sql);
        preparedStatement.setString(1, userId);
        preparedStatement.setString(2, id);
        preparedStatement.executeUpdate();
        return project;
    }

    @Override
    public void removeAll() throws SQLException {
        @NotNull final String sql = "DELETE FROM project";
        @NotNull final PreparedStatement preparedStatement = connection.prepareStatement(sql);
        preparedStatement.executeUpdate();
    }

    @Override
    public void removeAll(@NotNull String userId) throws SQLException {
        @NotNull final String sql = "DELETE FROM project WHERE userId=?";
        @NotNull final PreparedStatement preparedStatement = connection.prepareStatement(sql);
        preparedStatement.setString(1, userId);
        preparedStatement.executeUpdate();
    }

    @Override
    public boolean isNameExist(@NotNull String userId, @NotNull String name) throws SQLException {
        return (findName(userId, name) != null);
    }

    @Nullable
    private Project fetch(@NotNull final ResultSet resultSet) throws SQLException {
        @NotNull final Project project = new Project();
        if (resultSet.next()) {
            project.setId(resultSet.getString("id"));
            project.setName(resultSet.getString("name"));
            project.setDescription(resultSet.getString("description"));
            project.setUserId(resultSet.getString("userId"));
            project.setStatus(Status.valueOf(resultSet.getString("status")));
            if (resultSet.getTimestamp("createDate") != null)
                project.setCreateDate(new Date(resultSet.getTimestamp("createDate").getTime()));
            if (resultSet.getTimestamp("startDate") != null)
                project.setStartDate(new Date(resultSet.getTimestamp("startDate").getTime()));
            if (resultSet.getTimestamp("finishDate") != null)
                project.setFinishDate(new Date(resultSet.getTimestamp("finishDate").getTime()));
            return project;
        }
        return null;
    }
}