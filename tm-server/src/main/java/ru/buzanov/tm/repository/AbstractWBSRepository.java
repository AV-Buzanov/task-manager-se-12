package ru.buzanov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.buzanov.tm.api.repository.IWBSRepository;
import ru.buzanov.tm.constant.FormatConst;
import ru.buzanov.tm.entity.AbstractWBS;
import ru.buzanov.tm.enumerated.Field;

import java.sql.SQLException;
import java.util.Collection;

public abstract class AbstractWBSRepository<T extends AbstractWBS> extends AbstractRepository<T> implements IWBSRepository<T> {
    @NotNull
    public abstract Collection<T> findAll(@NotNull final String userId) throws SQLException;

    public abstract @NotNull Collection<T> findAllOrdered(@NotNull String userId, boolean dir, @NotNull Field field) throws SQLException;

    @NotNull
    public abstract Collection<T> findByName(@NotNull final String userId, @NotNull final String name) throws SQLException;

    @Nullable
    public abstract T findName(@NotNull final String userId, @NotNull final String name) throws SQLException;

    @NotNull
    public abstract Collection<T> findByDescription(@NotNull final String userId, @NotNull final String desc) throws SQLException;

    @Nullable
    public abstract T findOne(@NotNull final String userId, @NotNull final String id) throws SQLException;

    public abstract boolean isNameExist(@NotNull final String userId, @NotNull final String name) throws SQLException;

    public abstract void merge(@NotNull final String userId, @NotNull final String id, @NotNull final T entity) throws SQLException;

    @Nullable
    public abstract T remove(@NotNull final String userId, @NotNull final String id) throws SQLException;

    public abstract void removeAll(@NotNull final String userId) throws SQLException;
}
