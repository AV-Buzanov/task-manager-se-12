package ru.buzanov.tm.repository;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.buzanov.tm.api.repository.ITaskRepository;
import ru.buzanov.tm.entity.Task;
import ru.buzanov.tm.enumerated.Field;
import ru.buzanov.tm.enumerated.Status;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

@NoArgsConstructor
public class TaskRepository extends AbstractWBSRepository<Task> implements ITaskRepository {
    @Nullable
    @Override
    public Task load(@NotNull Task task) throws SQLException {
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        @NotNull final String sql =
                "INSERT INTO task (id, name, description, createDate, startDate, finishDate, userId, status, projectId) VALUES (?,?,?,?,?,?,?,?,?)";
        @NotNull final PreparedStatement preparedStatement = connection.prepareStatement(sql);
        preparedStatement.setString(1, task.getId());
        preparedStatement.setString(2, task.getName());
        preparedStatement.setString(3, task.getDescription());
        preparedStatement.setTimestamp(4, timestamp);
        timestamp = null;
        if (task.getStartDate() != null) timestamp = new Timestamp(task.getStartDate().getTime());
        preparedStatement.setTimestamp(5, timestamp);
        timestamp = null;
        if (task.getFinishDate() != null) timestamp = new Timestamp(task.getFinishDate().getTime());
        preparedStatement.setTimestamp(6, timestamp);
        preparedStatement.setString(7, task.getUserId());
        preparedStatement.setString(8, task.getStatus().toString());
        preparedStatement.setString(9, task.getProjectId());
        preparedStatement.executeUpdate();

        return task;
    }

    @Override
    public void load(@NotNull List<Task> list) throws SQLException {
        for (@NotNull final Task task : list) {
            load(task);
        }
    }

    public void merge(@NotNull final String id, @NotNull final Task task) throws SQLException {
        Timestamp timestamp;
        @NotNull final StringBuilder sql = new StringBuilder("UPDATE task SET name=?, startDate=?, finishDate=?, description=?, status=?, projectId=? WHERE id='");
        sql.append(id).append("'");
        @NotNull final PreparedStatement preparedStatement = connection.prepareStatement(sql.toString());
        preparedStatement.setString(1, task.getName());
        timestamp = null;
        if (task.getStartDate() != null) timestamp = new Timestamp(task.getStartDate().getTime());
        preparedStatement.setTimestamp(2, timestamp);
        timestamp = null;
        if (task.getFinishDate() != null) timestamp = new Timestamp(task.getFinishDate().getTime());
        preparedStatement.setTimestamp(3, timestamp);
        preparedStatement.setString(4, task.getDescription());
        if (task.getDescription() != null)
            preparedStatement.setString(5, task.getStatus().toString());
        else preparedStatement.setString(5, null);
        preparedStatement.setString(6, task.getProjectId());
        preparedStatement.executeUpdate();
    }

    public void merge(@NotNull final String userId, @NotNull final String id, @NotNull final Task task) throws SQLException {
        Timestamp timestamp;
        @NotNull final StringBuilder sql = new StringBuilder("UPDATE task SET name=?, startDate=?, finishDate=?, description=?, status=?, projectId=? WHERE id='");
        sql.append(id).append("' AND userId='").append(userId).append("'");
        @NotNull final PreparedStatement preparedStatement = connection.prepareStatement(sql.toString());
        preparedStatement.setString(1, task.getName());
        timestamp = null;
        if (task.getStartDate() != null) timestamp = new Timestamp(task.getStartDate().getTime());
        preparedStatement.setTimestamp(2, timestamp);
        timestamp = null;
        if (task.getFinishDate() != null) timestamp = new Timestamp(task.getFinishDate().getTime());
        preparedStatement.setTimestamp(3, timestamp);
        preparedStatement.setString(4, task.getDescription());
        if (task.getDescription() != null)
            preparedStatement.setString(5, task.getStatus().toString());
        else preparedStatement.setString(5, null);
        preparedStatement.setString(6, task.getProjectId());
        preparedStatement.executeUpdate();

    }

    @Nullable
    @Override
    public Task findOne(@NotNull String id) throws SQLException {
        @NotNull final String sql = "SELECT*FROM task WHERE id='" + id + "'";
        @NotNull final PreparedStatement preparedStatement = connection.prepareStatement(sql);
        ResultSet resultSet = preparedStatement.executeQuery();

        return fetch(resultSet);
    }

    @Nullable
    @Override
    public Task findOne(@NotNull String userId, @NotNull String id) throws SQLException {
        @NotNull final String sql = "SELECT*FROM task WHERE userId='" + userId + "' AND id='" + id + "'";
        Task task;
        @NotNull final PreparedStatement preparedStatement = connection.prepareStatement(sql);
        @NotNull final ResultSet resultSet = preparedStatement.executeQuery();
        task = fetch(resultSet);

        return task;
    }

    @Override
    public @NotNull Collection<Task> findAll() throws SQLException {
        @NotNull final String sql = "SELECT*FROM task";
        @NotNull final Collection<Task> list = new ArrayList<>();
        @NotNull final PreparedStatement preparedStatement = connection.prepareStatement(sql);
        @NotNull final ResultSet resultSet = preparedStatement.executeQuery();
        Task task;
        do {
            task = fetch(resultSet);
            if (task != null) list.add(task);
        } while (task != null);

        return list;
    }

    @Override
    public @NotNull Collection<Task> findAll(@NotNull String userId) throws SQLException {
        @NotNull final String sql = "SELECT*FROM task WHERE userId='" + userId + "'";
        @NotNull final Collection<Task> list = new ArrayList<>();
        @NotNull final PreparedStatement preparedStatement = connection.prepareStatement(sql);
        @NotNull final ResultSet resultSet = preparedStatement.executeQuery();
        Task task;
        do {
            task = fetch(resultSet);
            if (task != null) list.add(task);
        } while (task != null);

        return list;
    }

    public @NotNull Collection<Task> findAllOrdered(@NotNull String userId, boolean dir, @NotNull Field field) throws SQLException {
        @NotNull String sql = "SELECT*FROM task WHERE userId=? ORDER BY ?";
        if (!dir)
            sql = sql.concat(" DESC");
        @NotNull final Collection<Task> list = new ArrayList<>();
        @NotNull final PreparedStatement preparedStatement = connection.prepareStatement(sql);
        preparedStatement.setString(1, userId);
        preparedStatement.setString(2, field.displayName());
        @NotNull final ResultSet resultSet = preparedStatement.executeQuery();
        Task task;
        do {
            task = fetch(resultSet);
            if (task != null) list.add(task);
        } while (task != null);

        return list;
    }

    @Override
    public @NotNull Collection<Task> findByName(@NotNull String userId, @NotNull String name) throws SQLException {
        @NotNull final String sql = "SELECT*FROM task WHERE userId=? AND name LIKE ?";
        @NotNull final Collection<Task> list = new ArrayList<>();
        @NotNull final PreparedStatement preparedStatement = connection.prepareStatement(sql);
        preparedStatement.setString(1, userId);
        preparedStatement.setString(2, "%" + name + "%");
        @NotNull final ResultSet resultSet = preparedStatement.executeQuery();
        Task task;
        do {
            task = fetch(resultSet);
            if (task != null) list.add(task);
        } while (task != null);

        return list;
    }

    @Nullable
    @Override
    public Task findName(@NotNull String userId, @NotNull String name) throws SQLException {
        @NotNull final String sql = "SELECT*FROM task WHERE userId=? AND name=?";
        @NotNull final PreparedStatement preparedStatement = connection.prepareStatement(sql);
        preparedStatement.setString(1, userId);
        preparedStatement.setString(2, name);
        ResultSet resultSet = preparedStatement.executeQuery();

        return fetch(resultSet);
    }

    @Override
    public @NotNull Collection<Task> findByDescription(@NotNull String userId, @NotNull String desc) throws SQLException {
        @NotNull final String sql = "SELECT*FROM task WHERE userId=? AND description LIKE ?";
        @NotNull final Collection<Task> list = new ArrayList<>();
        @NotNull final PreparedStatement preparedStatement = connection.prepareStatement(sql);
        preparedStatement.setString(1, userId);
        preparedStatement.setString(2, "%" + desc + "%");
        @NotNull final ResultSet resultSet = preparedStatement.executeQuery();
        Task task;
        do {
            task = fetch(resultSet);
            if (task != null) list.add(task);
        } while (task != null);

        return list;
    }

    @NotNull
    public Collection<Task> findByProjectId(@NotNull final String projectId) throws SQLException {
        @NotNull final String sql = "SELECT*FROM task WHERE projectId=?";
        @NotNull final Collection<Task> list = new ArrayList<>();
        @NotNull final PreparedStatement preparedStatement = connection.prepareStatement(sql);
        preparedStatement.setString(1, projectId);
        @NotNull final ResultSet resultSet = preparedStatement.executeQuery();
        Task task;
        do {
            task = fetch(resultSet);
            if (task != null) list.add(task);
        } while (task != null);

        return list;
    }

    @NotNull
    public Collection<Task> findByProjectId(@NotNull final String userId, @NotNull final String projectId) throws SQLException {
        @NotNull final String sql = "SELECT*FROM task WHERE userId=? AND projectId=?";
        @NotNull final Collection<Task> list = new ArrayList<>();
        @NotNull final PreparedStatement preparedStatement = connection.prepareStatement(sql);
        preparedStatement.setString(1, userId);
        preparedStatement.setString(2, projectId);
        @NotNull final ResultSet resultSet = preparedStatement.executeQuery();
        Task task;
        do {
            task = fetch(resultSet);
            if (task != null) list.add(task);
        } while (task != null);

        return list;
    }

    @Nullable
    @Override
    public Task remove(@NotNull String userId, @NotNull String id) throws SQLException {
        @NotNull final String sql = "DELETE FROM task WHERE userId=? AND id=?";
        final Task task = findOne(userId, id);
        @NotNull final PreparedStatement preparedStatement = connection.prepareStatement(sql);
        preparedStatement.setString(1, userId);
        preparedStatement.setString(2, id);
        preparedStatement.executeUpdate();

        return task;
    }

    @Nullable
    @Override
    public Task remove(@NotNull String id) throws SQLException {
        @NotNull final String sql = "DELETE FROM task WHERE id=?";
        final Task task = findOne(id);
        @NotNull final PreparedStatement preparedStatement = connection.prepareStatement(sql);
        preparedStatement.setString(1, id);
        preparedStatement.executeUpdate();

        return task;
    }

    @Override
    public void removeAll() throws SQLException {
        @NotNull final String sql = "DELETE FROM task";
        @NotNull final PreparedStatement preparedStatement = connection.prepareStatement(sql);
        preparedStatement.executeUpdate();

    }

    @Override
    public void removeAll(@NotNull String userId) throws SQLException {
        @NotNull final String sql = "DELETE FROM task WHERE userId=?";
        @NotNull final PreparedStatement preparedStatement = connection.prepareStatement(sql);
        preparedStatement.setString(1, userId);
        preparedStatement.executeUpdate();

    }

    public void removeByProjectId(@NotNull final String projectId) throws SQLException {
        @NotNull final String sql = "DELETE FROM task WHERE projectId=?";
        @NotNull final PreparedStatement preparedStatement = connection.prepareStatement(sql);
        preparedStatement.setString(1, projectId);
        preparedStatement.executeUpdate();

    }

    public void removeByProjectId(@NotNull final String userId, @NotNull final String projectId) throws SQLException {
        String sql = "DELETE FROM task WHERE userId='" + userId + "' AND projectId='" + projectId + "'";
        PreparedStatement preparedStatement = connection.prepareStatement(sql);
        preparedStatement.executeUpdate();

    }

    @Override
    public boolean isNameExist(@NotNull String userId, @NotNull String name) throws SQLException {
        return (findName(userId, name) != null);
    }

    @Nullable
    private Task fetch(@NotNull ResultSet resultSet) throws SQLException {
        @NotNull final Task task = new Task();
        if (resultSet.next()) {
            task.setId(resultSet.getString("id"));
            task.setName(resultSet.getString("name"));
            task.setDescription(resultSet.getString("description"));
            task.setUserId(resultSet.getString("userId"));
            task.setStatus(Status.valueOf(resultSet.getString("status")));
            if (resultSet.getTimestamp("createDate") != null)
                task.setCreateDate(new Date(resultSet.getTimestamp("createDate").getTime()));
            if (resultSet.getTimestamp("startDate") != null)
                task.setStartDate(new Date(resultSet.getTimestamp("startDate").getTime()));
            if (resultSet.getTimestamp("finishDate") != null)
                task.setFinishDate(new Date(resultSet.getTimestamp("finishDate").getTime()));
            task.setProjectId(resultSet.getString("projectId"));
            return task;
        }
        return null;
    }
}
