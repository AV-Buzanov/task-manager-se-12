package ru.buzanov.tm.repository;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.buzanov.tm.api.repository.IRepository;
import ru.buzanov.tm.entity.AbstractEntity;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Collection;
import java.util.List;
@Getter
@Setter
public abstract class AbstractRepository<T extends AbstractEntity> implements IRepository<T> {
    @Nullable
    protected Connection connection;

    @Nullable
    public abstract T load(@NotNull final T entity) throws SQLException;

    public abstract void load(@NotNull final List<T> list) throws SQLException;

    @NotNull
    public abstract Collection<T> findAll() throws SQLException;

    @Nullable
    public abstract T findOne(@NotNull final String id) throws SQLException;

    public abstract void merge(@NotNull final String id, @NotNull final T entity) throws SQLException;

    @Nullable
    public abstract T remove(@NotNull final String id) throws SQLException;

    public abstract void removeAll() throws SQLException;
}
