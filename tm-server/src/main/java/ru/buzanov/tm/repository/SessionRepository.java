package ru.buzanov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.buzanov.tm.api.repository.ISessionRepository;
import ru.buzanov.tm.entity.Session;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class SessionRepository extends AbstractRepository<Session> implements ISessionRepository {
    @Nullable
    @Override
    public Session load(@NotNull Session session) throws SQLException {
        @NotNull final String sql = "INSERT INTO session (id, userId, signature, createDate) VALUES (?,?,?,?)";
        @NotNull final PreparedStatement preparedStatement = connection.prepareStatement(sql);
        preparedStatement.setString(1, session.getId());
        preparedStatement.setString(2, session.getUserId());
        preparedStatement.setString(3, session.getSignature());
        Timestamp timestamp = null;
        if (session.getCreateDate() != null) timestamp = new Timestamp(session.getCreateDate().getTime());
        preparedStatement.setTimestamp(4, timestamp);
        preparedStatement.executeUpdate();

        return session;
    }

    @Override
    public void load(@NotNull List<Session> list) throws SQLException {
        for (@NotNull final Session session : list)
            load(session);
    }

    @Override
    public void merge(@NotNull String id, @NotNull Session session) throws SQLException {
        Timestamp timestamp;
        @NotNull final StringBuilder sql = new StringBuilder("UPDATE session SET userId=?, createDate=?, signature=? WHERE id='").append(id).append("'");
        @NotNull final PreparedStatement preparedStatement = connection.prepareStatement(sql.toString());
        preparedStatement.setString(1, session.getUserId());
        timestamp = null;
        if (session.getCreateDate() != null) timestamp = new Timestamp(session.getCreateDate().getTime());
        preparedStatement.setTimestamp(2, timestamp);
        preparedStatement.setString(3, session.getSignature());
        preparedStatement.executeUpdate();

    }

    @Nullable
    @Override
    public Session findOne(@NotNull String id) throws SQLException {
        @NotNull final String sql = "SELECT*FROM session WHERE id='" + id + "'";
        @NotNull final PreparedStatement preparedStatement = connection.prepareStatement(sql);
        ResultSet resultSet = preparedStatement.executeQuery();

        return fetch(resultSet);
    }

    @Override
    public @NotNull Collection<Session> findAll() throws SQLException {
        @NotNull final String sql = "SELECT*FROM session";
        @NotNull final Collection<Session> list = new ArrayList<>();
        @NotNull final PreparedStatement preparedStatement = connection.prepareStatement(sql);
        @NotNull final ResultSet resultSet = preparedStatement.executeQuery();
        Session session;
        do {
            session = fetch(resultSet);
            if (session != null) list.add(session);
        } while (session != null);

        return list;
    }

    @Nullable
    @Override
    public Session remove(@NotNull String id) throws SQLException {
        @NotNull final String sql = "DELETE FROM session WHERE id='" + id + "'";
        final Session session = findOne(id);
        @NotNull final PreparedStatement preparedStatement = connection.prepareStatement(sql);
        preparedStatement.executeUpdate();

        return session;
    }

    @Override
    public void removeAll() throws SQLException {
        @NotNull final String sql = "DELETE FROM session";
        @NotNull final PreparedStatement preparedStatement = connection.prepareStatement(sql);
        preparedStatement.executeUpdate();

    }

    @Nullable
    private Session fetch(@NotNull final ResultSet resultSet) throws SQLException {
        @NotNull final Session session = new Session();
        if (resultSet.next()) {
            session.setId(resultSet.getString("id"));
            session.setUserId(resultSet.getString("userId"));
            session.setSignature(resultSet.getString("signature"));
            if (resultSet.getTimestamp("createDate") != null)
                session.setCreateDate(new java.util.Date(resultSet.getTimestamp("createDate").getTime()));
            return session;
        }
        return null;
    }
}
