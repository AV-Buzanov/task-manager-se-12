package ru.buzanov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.buzanov.tm.command.AbstractCommand;
import ru.buzanov.tm.endpoint.RoleType;
import ru.buzanov.tm.endpoint.Session;
import ru.buzanov.tm.endpoint.User;

public class UserRemoveCommand extends AbstractCommand {
    @NotNull
    @Override
    public String command() {
        return "user-remove";
    }

    @NotNull
    @Override
    public String description() {
        return "Remove selected user";
    }

    @Override
    public void execute() throws Exception {
        terminalService.printLineG("[CHOOSE USER TO REMOVE]");
        @NotNull final Session session = serviceLocator.getCurrentSession();
        @NotNull final User[] users = new User[adminUserEndpoint.findByRole(session, RoleType.ПОЛЬЗОВАТЕЛЬ).size()];
        adminUserEndpoint.findByRole(session, RoleType.ПОЛЬЗОВАТЕЛЬ).toArray(users);
        for (int i = 0; i < users.length; i++) {
            terminalService.print(String.valueOf(i + 1));
            terminalService.printLine(": ", users[i].getLogin(), " " + users[i].getName());
        }
        int indexBuf = Integer.parseInt(terminalService.readLine()) - 1;
        terminalService.printLineG("[ARE YOU SURE(Y/N)?]");
        if ("Y".equals(terminalService.readLine())) {
            adminUserEndpoint.removeAdmin(session, users[indexBuf].getId());
            terminalService.printLineG("[USER REMOVED]");
        } else
            terminalService.printLineG("[CANCELED]");
    }

    @Override
    public boolean isSecure() throws Exception {
        return true;
    }

    @Override
    public boolean isRoleAllow(RoleType role) {
        if (RoleType.ПОЛЬЗОВАТЕЛЬ.equals(role))
            return false;
        return super.isRoleAllow(role);
    }
}
