package ru.buzanov.tm.command.data;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import org.jetbrains.annotations.NotNull;
import ru.buzanov.tm.command.AbstractCommand;
import ru.buzanov.tm.constant.FormatConst;
import ru.buzanov.tm.dto.EntityDTO;

import java.io.File;
import java.io.FileOutputStream;

public class DataJackXmlSaveCommand extends AbstractCommand {
    @Override
    public @NotNull String command() {
        return "data-jackxml-save";
    }

    @Override
    public @NotNull String description() {
        return "Save data in jackson xml format.";
    }

    @Override
    public void execute() throws Exception {
        @NotNull final ObjectMapper mapper = new XmlMapper();
        @NotNull final EntityDTO dto = new EntityDTO();
        dto.load(serviceLocator);
        @NotNull final File path = new File(FormatConst.SAVE_PATH + "JData.xml");
        if (!path.exists()) {
            path.getParentFile().mkdirs();
            path.createNewFile();
        }
        try (@NotNull final FileOutputStream outputStream = new FileOutputStream(path)) {
            mapper.writerWithDefaultPrettyPrinter().writeValue(outputStream, dto);
            terminalService.printLineG("[DATA SAVED]");
        }
    }

    @Override
    public boolean isSecure() throws Exception {
        return false;
    }
}
