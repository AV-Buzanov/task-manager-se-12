package ru.buzanov.tm.command.data;

import org.eclipse.persistence.jaxb.UnmarshallerProperties;
import org.jetbrains.annotations.NotNull;
import ru.buzanov.tm.command.AbstractCommand;
import ru.buzanov.tm.constant.FormatConst;
import ru.buzanov.tm.dto.EntityDTO;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import javax.xml.transform.stream.StreamSource;
import java.io.File;
import java.io.FileInputStream;

public class DataJsonLoadCommand extends AbstractCommand {

    @NotNull
    @Override
    public String command() {
        return "data-json-load";
    }

    @NotNull
    @Override
    public String description() {
        return "Load data json";
    }

    @Override
    public void execute() throws Exception {
        System.setProperty("javax.xml.bind.context.factory", "org.eclipse.persistence.jaxb.JAXBContextFactory");
        @NotNull final JAXBContext jaxbContext = JAXBContext.newInstance(EntityDTO.class);
        @NotNull final Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
        unmarshaller.setProperty(UnmarshallerProperties.MEDIA_TYPE, "application/json");
        unmarshaller.setProperty(UnmarshallerProperties.JSON_INCLUDE_ROOT, true);
        @NotNull final File path = new File(FormatConst.SAVE_PATH + "Data.json");
        @NotNull final StreamSource inputStream = new StreamSource(new FileInputStream(path));
        @NotNull final EntityDTO dto = (EntityDTO) unmarshaller.unmarshal(inputStream);
        projectService.loadListP(serviceLocator.getCurrentSession(), dto.getProjects());
        taskService.loadListT(serviceLocator.getCurrentSession(), dto.getTasks());
        terminalService.printLineG("[DATA LOADED]");
    }

    @Override
    public boolean isSecure() throws Exception {
        return false;
    }
}
