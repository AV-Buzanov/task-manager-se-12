package ru.buzanov.tm.command;

import org.jetbrains.annotations.NotNull;
import ru.buzanov.tm.api.ServiceLocator;


import ru.buzanov.tm.api.service.ITerminalService;
import ru.buzanov.tm.endpoint.*;
import ru.buzanov.tm.endpoint.Exception;

public abstract class AbstractCommand {
    protected ServiceLocator serviceLocator;

    protected ITerminalService terminalService;

    protected UserEndpoint userService;

    protected ProjectEndpoint projectService;

    protected TaskEndpoint taskService;

    protected SessionEndpoint sessionService;

    protected AdminUserEndpoint adminUserEndpoint;

    public void setServiceLocator(@NotNull ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
        this.terminalService = serviceLocator.getTerminalService();
        this.userService = serviceLocator.getUserEndPoint();
        this.projectService = serviceLocator.getProjectEndpoint();
        this.taskService = serviceLocator.getTaskEndpoint();
        this.sessionService = serviceLocator.getSessionEndpoint();
        this.adminUserEndpoint = serviceLocator.getAdminUserEndpoint();
    }

    @NotNull
    public abstract String command();

    @NotNull
    public abstract String description();

    public abstract void execute() throws Exception, java.lang.Exception;

    public abstract boolean isSecure() throws Exception, java.lang.Exception;

    public boolean isRoleAllow(RoleType role) {
        return true;
    }
}