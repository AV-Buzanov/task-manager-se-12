package ru.buzanov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.buzanov.tm.command.AbstractCommand;
import ru.buzanov.tm.endpoint.User;

import java.util.Objects;

public class UserViewCommand extends AbstractCommand {
    @NotNull
    @Override
    public String command() {
        return "user-view";
    }

    @NotNull
    @Override
    public String description() {
        return "View user information";
    }

    @Override
    public void execute() throws Exception {
        terminalService.printLineG("[VIEW USER]");
        @NotNull final User user = serviceLocator.getCurrentUser();
        terminalService.printLineG("[LOGIN]");
        terminalService.printLine(user.getLogin());
        terminalService.printLineG("[NAME]");
        terminalService.printLine(user.getName());
        terminalService.printLineG("[ROLE]");
        terminalService.printLine(user.getRoleType().value());
        terminalService.printLineG("[PROJECTS COUNT]");
        terminalService.printLine(String.valueOf(Objects.requireNonNull(projectService.findAllP(serviceLocator.getCurrentSession())).size()));
        terminalService.printLineG("[TASKS COUNT]");
        terminalService.printLine(String.valueOf(Objects.requireNonNull(taskService.findAllT(serviceLocator.getCurrentSession())).size()));
    }

    @Override
    public boolean isSecure() throws Exception {
        return true;
    }
}
