package ru.buzanov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.buzanov.tm.command.AbstractCommand;
import ru.buzanov.tm.endpoint.Project;
import ru.buzanov.tm.endpoint.Session;
import ru.buzanov.tm.endpoint.Task;

import java.util.Objects;

public class TaskListCommand extends AbstractCommand {
    @NotNull
    @Override
    public String command() {
        return "task-list";
    }

    @NotNull
    @Override
    public String description() {
        return "Show all tasks.";
    }

    @Override
    public void execute() throws Exception {
        terminalService.printLineG("[TASK LIST]");
        @NotNull final Session session = serviceLocator.getCurrentSession();
        for (@NotNull final Task task : Objects.requireNonNull(taskService.findAllT(session))) {
            terminalService.printWBS(task);
            terminalService.printG(" [PROJECT] ");
            Project project = projectService.findOneP(session, task.getProjectId());
            if (project != null)
                terminalService.printLine(project.getName());
        }
    }

    @Override
    public boolean isSecure() throws Exception {
        return true;
    }
}
