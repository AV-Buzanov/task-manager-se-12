package ru.buzanov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.buzanov.tm.command.AbstractCommand;
import ru.buzanov.tm.endpoint.Session;
import ru.buzanov.tm.endpoint.Task;

public class TaskViewCommand extends AbstractCommand {
    @NotNull
    @Override
    public String command() {
        return "task-view";
    }

    @NotNull
    @Override
    public String description() {
        return "View task information";
    }

    @Override
    public void execute() throws Exception {
        terminalService.printLineG("[CHOOSE TASK TO VIEW]");
        @NotNull final Session session = serviceLocator.getCurrentSession();
        terminalService.printLine(taskService.getListT(session));
        @Nullable String idBuf = taskService.getIdByCountT(session, Integer.parseInt(terminalService.readLine()));
        @Nullable final Task task = taskService.findOneT(session, idBuf);
        terminalService.printWBSLine(task);
        terminalService.printG("[PROJECT]");
        terminalService.printLine(projectService.findOneP(session, task.getProjectId()).getName());
    }

    @Override
    public boolean isSecure() throws Exception {
        return true;
    }
}
