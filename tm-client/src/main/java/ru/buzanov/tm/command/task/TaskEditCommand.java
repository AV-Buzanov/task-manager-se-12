package ru.buzanov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.buzanov.tm.command.AbstractCommand;
import ru.buzanov.tm.endpoint.Session;
import ru.buzanov.tm.endpoint.Task;

import java.util.Objects;

public class TaskEditCommand extends AbstractCommand {
    @NotNull
    @Override
    public String command() {
        return "task-edit";
    }

    @NotNull
    @Override
    public String description() {
        return "Edit task";
    }

    @Override
    public void execute() throws Exception {
        @NotNull final Session session = serviceLocator.getCurrentSession();
        terminalService.printLineG("[CHOOSE TASK TO EDIT]");
        terminalService.printLine(taskService.getListT(session));
        @Nullable String stringBuf = taskService.getIdByCountT(session, Integer.parseInt(terminalService.readLine()));
        @NotNull final Task task = Objects.requireNonNull(taskService.findOneT(session, stringBuf));
        terminalService.printLineG("[NAME]");
        terminalService.printLine(task.getName());
        terminalService.printLineG("[ENTER NEW NAME]");
        stringBuf = terminalService.readLine();
        if (!stringBuf.isEmpty()&&taskService.isNameExistT(serviceLocator.getCurrentSession(), stringBuf)) {
            terminalService.printLineR("This name already exist.");
            return;
        }
        if (!stringBuf.isEmpty())
        task.setName(stringBuf);
        terminalService.readWBS(task);
        terminalService.printLineG("[PROJECT]");
        terminalService.printLine(projectService.findOneP(session, task.getProjectId()).getName());
        terminalService.printLineG("[CHOOSE NEW PROJECT, WRITE 0 TO REMOVE]");
        terminalService.printLine(projectService.getListP(session));
        stringBuf = terminalService.readLine();
        if (!stringBuf.isEmpty()) {
            if ("0".equals(stringBuf))
                stringBuf = null;
            else stringBuf = projectService.getIdByCountP(session, Integer.parseInt(stringBuf));
            task.setProjectId(stringBuf);
        }
        taskService.mergeT(session, task.getId(), task);
        terminalService.printLineG("[OK]");
    }

    @Override
    public boolean isSecure() throws Exception {
        return true;
    }
}
