package ru.buzanov.tm.command.data;

import org.jetbrains.annotations.NotNull;
import ru.buzanov.tm.command.AbstractCommand;
import ru.buzanov.tm.constant.FormatConst;
import ru.buzanov.tm.dto.EntityDTO;

import java.io.File;
import java.io.FileInputStream;
import java.io.ObjectInputStream;

public class DataBinLoadCommand extends AbstractCommand {
    @NotNull
    @Override
    public String command() {
        return "data-bin-load";
    }

    @NotNull
    @Override
    public String description() {
        return "Load data from binary file.";
    }

    @Override
    public void execute() throws Exception {
        @NotNull final File file = new File(FormatConst.SAVE_PATH + "Data.bin");

        try (@NotNull final ObjectInputStream objectInputStream = new ObjectInputStream(new FileInputStream(file))) {
            @NotNull final EntityDTO dto = (EntityDTO) objectInputStream.readObject();
            projectService.loadListP(serviceLocator.getCurrentSession(), dto.getProjects());
            taskService.loadListT(serviceLocator.getCurrentSession(), dto.getTasks());
            terminalService.printLineG("[DATA LOADED]");
        }
    }

    @Override
    public boolean isSecure() throws Exception {
        return false;
    }
}
