package ru.buzanov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.buzanov.tm.command.AbstractCommand;
import ru.buzanov.tm.endpoint.Session;

public class UserAuthCommand extends AbstractCommand {
    @NotNull
    @Override
    public String command() {
        return "auth";
    }

    @NotNull
    @Override
    public String description() {
        return "User authentication";
    }

    @Override
    public void execute() throws Exception {
        if (serviceLocator.getCurrentSession() != null) {
            terminalService.printLineG("[LOG OUT BEFORE AUTHORISATION]");
            return;
        }
        terminalService.printLineG("[AUTHORISATION]");
        terminalService.printLineG("[ENTER LOGIN]");
        @NotNull final String login = terminalService.readLine();
        if (!userService.isLoginExist(login)) {
            terminalService.printLineR("This login doesn't exist.");
            return;
        }
        terminalService.printLineG("[ENTER PASS]");
        @NotNull final String pass = terminalService.readLine();
        @NotNull final Session session = sessionService.getSession(login, pass);
        serviceLocator.setCurrentSession(session);
        serviceLocator.setCurrentUser(userService.findOne(session));
        terminalService.printLineG("[HELLO, " + serviceLocator.getCurrentUser().getName() + ", NICE TO SEE YOU!]");
    }

    @Override
    public boolean isSecure() throws Exception {
        return false;
    }
}
