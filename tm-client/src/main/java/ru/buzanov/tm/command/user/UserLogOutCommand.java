package ru.buzanov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.buzanov.tm.command.AbstractCommand;
import ru.buzanov.tm.endpoint.Session;

public class UserLogOutCommand extends AbstractCommand {
    @NotNull
    @Override
    public String command() {
        return "log-out";
    }

    @NotNull
    @Override
    public String description() {
        return "User log-out";
    }

    @Override
    public void execute() throws Exception {
        @Nullable final Session session = serviceLocator.getCurrentSession();
        serviceLocator.setCurrentUser(null);
        serviceLocator.setCurrentSession(null);
        sessionService.killSession(session);
        terminalService.printLineG("[YOU ARE UNAUTHORIZED NOW]");
    }

    @Override
    public boolean isSecure() throws Exception {
        return true;
    }
}
