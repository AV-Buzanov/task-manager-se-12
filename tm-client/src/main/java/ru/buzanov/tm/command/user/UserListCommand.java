package ru.buzanov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.buzanov.tm.command.AbstractCommand;
import ru.buzanov.tm.endpoint.RoleType;
import ru.buzanov.tm.endpoint.Session;
import ru.buzanov.tm.endpoint.User;

public class UserListCommand extends AbstractCommand {
    @NotNull
    @Override
    public String command() {
        return "user-list";
    }

    @NotNull
    @Override
    public String description() {
        return "View user list (for admin only)";
    }

    @Override
    public void execute() throws Exception {
        terminalService.printLineG("[USER LIST]");
        @NotNull final Session session = serviceLocator.getCurrentSession();
        for (@NotNull final User user : adminUserEndpoint.findAll(session)) {
            terminalService.printG("[LOGIN] ");
            terminalService.print(user.getLogin());
            terminalService.printG(" [NAME] ");
            terminalService.print(user.getName());
            terminalService.printG(" [ROLE] ");
            terminalService.printLine(user.getRoleType().value());
        }
    }

    @Override
    public boolean isSecure() throws Exception {
        return true;
    }

    @Override
    public boolean isRoleAllow(RoleType role) {
        if (RoleType.ПОЛЬЗОВАТЕЛЬ.equals(role))
            return false;
        return super.isRoleAllow(role);
    }
}
