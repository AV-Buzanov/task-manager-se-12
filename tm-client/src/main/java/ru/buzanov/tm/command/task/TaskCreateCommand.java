package ru.buzanov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.buzanov.tm.command.AbstractCommand;
import ru.buzanov.tm.endpoint.Session;
import ru.buzanov.tm.endpoint.Task;

public class TaskCreateCommand extends AbstractCommand {
    @NotNull
    @Override
    public String command() {
        return "task-create";
    }

    @NotNull
    @Override
    public String description() {
        return "Create new task.";
    }

    @Override
    public void execute() throws Exception {
        @NotNull Task task = new Task();
        @NotNull final Session session = serviceLocator.getCurrentSession();
        terminalService.printLineG("[TASK CREATE]");
        terminalService.printG("[ENTER NAME]");
        @NotNull String stringBuf = terminalService.readLine();
        if (taskService.isNameExistT(serviceLocator.getCurrentSession(), stringBuf)) {
            terminalService.printLineR("This name already exist.");
            return;
        }
        task.setName(stringBuf);
        terminalService.printLine("[FILL DATA(Y/N)?]");
        if (terminalService.readLine().equals("Y"))
            terminalService.readWBS(task);
        if (!projectService.findAllP(session).isEmpty()) {
            terminalService.printLineG("[CHOOSE PROJECT]");
            terminalService.printLine(projectService.getListP(session));
            stringBuf = terminalService.readLine();
            if (!stringBuf.isEmpty()) {
                task.setProjectId(projectService.getIdByCountP(session, Integer.parseInt(stringBuf)));
            }
        }
        taskService.loadT(session, task);
        terminalService.printLineG("[OK]");
    }

    @Override
    public boolean isSecure() throws Exception {
        return true;
    }
}
