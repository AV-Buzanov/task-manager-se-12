package ru.buzanov.tm.util;

import org.fusesource.jansi.AnsiConsole;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.buzanov.tm.api.service.ITerminalService;
import ru.buzanov.tm.constant.FormatConst;
import ru.buzanov.tm.endpoint.AbstractWBS;
import ru.buzanov.tm.endpoint.Status;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Comparator;

import static org.fusesource.jansi.Ansi.Color.GREEN;
import static org.fusesource.jansi.Ansi.Color.RED;
import static org.fusesource.jansi.Ansi.ansi;

public class TerminalService implements ITerminalService {
    private BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

    @NotNull
    public String readLine() throws IOException {
        return reader.readLine();
    }

    public void printWBS(@Nullable AbstractWBS entity) {
        if (entity == null)
            return;
        printG("[NAME] ");
        print(entity.getName());
        printG(" [DESCRIPTION] ");
        print(entity.getDescription());
        printG(" [CREATE DATE] ");
        String date = null;
        if (entity.getCreateDate() != null)
            date = DateUtil.dateFormat().format(DateUtil.toDate(entity.getCreateDate()));
        print(date);
        printG(" [START DATE] ");
        date = null;
        if (entity.getStartDate() != null) date = DateUtil.dateFormat().format(DateUtil.toDate(entity.getStartDate()));
        print(date);
        printG(" [END DATE] ");
        date = null;
        if (entity.getFinishDate() != null)
            date = DateUtil.dateFormat().format(DateUtil.toDate(entity.getFinishDate()));
        print(date);
        printG(" [STATUS] ");
        print(entity.getStatus().value());
    }

    public void printWBSLine(@Nullable AbstractWBS entity) {
        if (entity == null)
            return;
        printG("[NAME] ");
        printLine(entity.getName());
        printG("[DESCRIPTION] ");
        printLine(entity.getDescription());
        printG("[CREATE DATE] ");
        String date = null;
        if (entity.getCreateDate() != null)
            date = DateUtil.dateFormat().format(DateUtil.toDate(entity.getCreateDate()));
        printLine(date);
        printG("[START DATE] ");
        date = null;
        if (entity.getStartDate() != null) date = DateUtil.dateFormat().format(DateUtil.toDate(entity.getStartDate()));
        printLine(date);
        printG("[END DATE] ");
        date = null;
        if (entity.getFinishDate() != null)
            date = DateUtil.dateFormat().format(DateUtil.toDate(entity.getFinishDate()));
        printLine(date);
        printG("[STATUS] ");
        printLine(entity.getStatus().value());
    }

    public void readWBS(@Nullable AbstractWBS entity) throws Exception {
        if (entity == null)
            return;
        String stringBuf;
        printG("[ENTER START DATE] ");
        stringBuf = readLine();
        if (!stringBuf.isEmpty())
            entity.setStartDate(DateUtil.toXMLGregorianCalendar(DateUtil.dateFormat().parse(stringBuf)));
        printG("[ENTER END DATE] ");
        stringBuf = readLine();
        if (!stringBuf.isEmpty())
            entity.setFinishDate(DateUtil.toXMLGregorianCalendar(DateUtil.dateFormat().parse(stringBuf)));
        printG("[ENTER DESCRIPTION] ");
        stringBuf = readLine();
        if (!stringBuf.isEmpty())
            entity.setDescription(stringBuf);
        printLineG("[CHOOSE STATUS] ");
        int i = 1;
        for (Status st : Status.values()) {
            printLine(String.valueOf(i), ": ", st.value());
            i++;
        }
        stringBuf = readLine();
        if (!stringBuf.isEmpty()) {
            i = 1;
            for (Status st : Status.values()) {
                if (i == Integer.parseInt(stringBuf))
                    entity.setStatus(st);
                i++;
            }
        }
    }

    public void print(@Nullable final String... strings) {
        StringBuilder stringBuilder = new StringBuilder();
        for (String string : strings) {
            if (string == null) stringBuilder.append(FormatConst.EMPTY_FIELD);
            else stringBuilder.append(string);
        }
        System.out.print(stringBuilder.toString());
    }

    public void printG(@Nullable final String... strings) {
        AnsiConsole.systemInstall();
        StringBuilder stringBuilder = new StringBuilder();
        for (String string : strings) {
            if (string == null) stringBuilder.append(FormatConst.EMPTY_FIELD);
            else stringBuilder.append(string);
        }
        System.out.print(ansi().fg(GREEN).a(stringBuilder.toString()).reset());
        AnsiConsole.systemUninstall();
    }

    public void printLine() {
        System.out.println();
    }

    public void printLine(@Nullable final String... strings) {
        StringBuilder stringBuilder = new StringBuilder();
        for (String string : strings) {
            if (string == null) stringBuilder.append(FormatConst.EMPTY_FIELD);
            else stringBuilder.append(string);
        }
        System.out.println(stringBuilder.toString());
    }

    public void printLineG(@Nullable final String... strings) {
        AnsiConsole.systemInstall();
        StringBuilder stringBuilder = new StringBuilder();
        for (String string : strings) {
            if (string == null) stringBuilder.append(FormatConst.EMPTY_FIELD);
            else stringBuilder.append(string);
        }
        System.out.println(ansi().fg(GREEN).a(stringBuilder.toString()).reset());
        AnsiConsole.systemUninstall();
    }

    public void printLineR(@Nullable final String... strings) {
        AnsiConsole.systemInstall();
        StringBuilder stringBuilder = new StringBuilder();
        for (String string : strings) {
            if (string == null) stringBuilder.append(FormatConst.EMPTY_FIELD);
            else stringBuilder.append(string);
        }
        System.out.println(ansi().fg(RED).a(stringBuilder.toString()).reset());
        AnsiConsole.systemUninstall();
    }

    @NotNull
    public Comparator<AbstractWBS> getStatusComparator(final boolean direction) {
        return new Comparator<AbstractWBS>() {
            @Override
            public int compare(AbstractWBS o1, AbstractWBS o2) {
                if (direction)
                    return o1.getStatus().compareTo(o2.getStatus());
                return o1.getStatus().compareTo(o2.getStatus()) * (-1);
            }
        };
    }
}
