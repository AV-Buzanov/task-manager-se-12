package ru.buzanov.tm.bootstrap;

import org.jetbrains.annotations.NotNull;
import ru.buzanov.tm.api.ServiceLocator;
import ru.buzanov.tm.endpoint.Exception_Exception;
import ru.buzanov.tm.endpoint.Session;

public class SessionUpdateThread extends Thread {
    @NotNull
    private final ServiceLocator serviceLocator;

    public SessionUpdateThread(@NotNull final ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @Override
    public void run() {
        while (true) {
            try {
                sleep(120000);
                if (serviceLocator.getCurrentSession() != null) {
                    @NotNull final Session session = serviceLocator.getSessionEndpoint().updateSession(serviceLocator.getCurrentSession());
                    serviceLocator.setCurrentSession(session);
                }
            } catch (Exception_Exception | InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
