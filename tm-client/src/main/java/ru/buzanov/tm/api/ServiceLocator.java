package ru.buzanov.tm.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.buzanov.tm.api.service.ITerminalService;
import ru.buzanov.tm.command.AbstractCommand;
import ru.buzanov.tm.endpoint.*;

import java.util.Collection;

public interface ServiceLocator {

    @NotNull Collection<AbstractCommand> getCommands();

    @NotNull ITerminalService getTerminalService();

    @NotNull ProjectEndpoint getProjectEndpoint();

    @NotNull TaskEndpoint getTaskEndpoint();

    @NotNull UserEndpoint getUserEndPoint();

    @NotNull SessionEndpoint getSessionEndpoint();

    @NotNull AdminUserEndpoint getAdminUserEndpoint();

    @Nullable Session getCurrentSession();

    @Nullable User getCurrentUser();

    void connect();

    void setCurrentSession(@Nullable final Session session);

    void setCurrentUser(@Nullable final User user);

    void setProjectEndpoint(@NotNull final ProjectEndpoint projectEndpoint);

    void setTaskEndpoint(@NotNull final TaskEndpoint taskEndpoint);

    void setUserEndPoint(@NotNull final UserEndpoint userEndPoint);

    void setSessionEndpoint(@NotNull final SessionEndpoint sessionEndpoint);

    void setAdminUserEndpoint(@NotNull final AdminUserEndpoint adminUserEndpoint);
}
