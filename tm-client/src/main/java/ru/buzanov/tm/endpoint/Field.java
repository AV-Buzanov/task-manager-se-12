
package ru.buzanov.tm.endpoint;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for field.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="field"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="name"/&gt;
 *     &lt;enumeration value="description"/&gt;
 *     &lt;enumeration value="startDate"/&gt;
 *     &lt;enumeration value="finishDate"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "field")
@XmlEnum
public enum Field {

    @XmlEnumValue("name")
    NAME("name"),
    @XmlEnumValue("description")
    DESCRIPTION("description"),
    @XmlEnumValue("startDate")
    START_DATE("startDate"),
    @XmlEnumValue("finishDate")
    FINISH_DATE("finishDate");
    private final String value;

    Field(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static Field fromValue(String v) {
        for (Field c: Field.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
