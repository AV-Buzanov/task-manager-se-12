package ru.buzanov.tm;

import ru.buzanov.tm.bootstrap.Bootstrap;

/**
 * Task Manager
 */

public final class App {
    public static void main(String[] args) {
        Bootstrap bootstrap = new Bootstrap();
        bootstrap.start();
    }
}